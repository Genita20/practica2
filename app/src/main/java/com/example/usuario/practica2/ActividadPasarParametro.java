package com.example.usuario.practica2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActividadPasarParametro extends AppCompatActivity {

    EditText cajaDatos;
    Button botonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pasar_parametro);

        cajaDatos = (EditText) findViewById(R.id.txtParametro);
        botonEnviar = (Button) findViewById(R.id.btnEnviarParametro);
        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPasarParametro.this, ActividadRecibirParametro.class);
                //se crea un objeto tipo Bundle que será la bitácora de parámetros a enviar
                Bundle bundle = new Bundle();
                // el método put fija los parametros a enviar mediante un id
                bundle.putString("dato",cajaDatos.getText().toString());
                // método putExtras envía un objeto de tipo bundle como un sólo parámetro entre actividades
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
