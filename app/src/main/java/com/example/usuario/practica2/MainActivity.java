package com.example.usuario.practica2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button botonLogin,botonRegistrar,botonBuscar,botonParametro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tomar el control de los elementos gráficos

        botonLogin = (Button) findViewById(R.id.Login);
        botonRegistrar = (Button) findViewById(R.id.Registrar);
        botonBuscar = (Button) findViewById(R.id.Buscar);
        botonParametro = (Button) findViewById(R.id.PasarParametro);

        // se generan los eventos en los botones

        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadPasarParametro.class);
                startActivity(intent);
            }
        });

        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Crear un objeto de la clase Intent (navegar)
                Intent intent = new Intent (MainActivity.this, ActividadLogin.class);
                startActivity(intent);
            }
        });

    }
}
